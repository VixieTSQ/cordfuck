module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				background: '#0E0705',
				backgroundEmmcare: '#2D2432',
				theme: '#940606',
				themeAlt: '#600000',
				themeEmmcare: '#B677A0',
				themeAltEmmcare: '#73345D',
				textColor: 'whitesmoke',
				textOff: '#b2b2b2',
				textOffish: '#d8d8d8'
			},
			backgroundImage: {
				pattern: 'url("$lib/assets/bg-pattern.svg")',
				emmcare: 'url("$lib/assets/emmcare-background.webp")',
				chainsawknightBg: 'url("$lib/assets/chainsawknight/chainsawbg.webp")',
				emmcareHeaderBg: 'url("$lib/assets/emmcare/haii.webp")'
			},

			screens: {
				xl: '1100px'
			}
		}
	},
	plugins: [require('tailwind-scrollbar')]
};
