import type { CheckoutState, Product, Release, Song } from '$lib/types.js';
import { get, writable } from 'svelte/store';
import { browser } from '$app/environment';

export type CurrentSong = {
	release: Release;
	song: Song;
	isPlaying: boolean;
};

const createCurrentSongStore = () => {
	const { subscribe, update, set } = writable<CurrentSong | undefined>();

	return {
		subscribe,
		decrementTrackNumber: () => {
			update((val) => {
				if (val) {
					const newSong = val.release.songs.find(
						(el) => el.trackNumber === val.song.trackNumber - 1
					);
					if (newSong) {
						val.song = newSong;
					}
				}
				return val;
			});
		},
		incrementTrackNumber: () => {
			update((val) => {
				if (val) {
					const newSong = val.release.songs.find(
						(el) => el.trackNumber === val.song.trackNumber + 1
					);
					if (newSong) {
						val.song = newSong;
					} else {
						return undefined;
					}
				}
				return val;
			});
		},
		pause: () => {
			update((val) => {
				if (val) val.isPlaying = false;
				return val;
			});
		},
		play: () => {
			update((val) => {
				if (val) val.isPlaying = true;
				return val;
			});
		},
		set
	};
};

const createCheckoutStateStore = () => {
	const { subscribe, set } = writable<CheckoutState>({
		decrementStage: () => {},
		ratePreview: undefined,
		shippingForm: undefined,
		orderId: undefined,
		stage: { kind: 'Cart' }
	});

	return {
		subscribe,
		set
	};
};

const createCartStore = async () => {
	const rawStoredCart = JSON.parse(browser ? localStorage.getItem('cart') ?? '[]' : '[]');
	let storedCart: { product: Product }[] = [];
	await Promise.all(
		rawStoredCart.map(async (elm: { id: string }) => {
			const product = await (await fetch(`/api/products.json?id=${elm.id}`)).json();
			if (!Array.isArray(product)) {
				storedCart = [...storedCart, { product: product }];
			}
		})
	);
	const { subscribe, update, set } = writable<{ product: Product }[]>(storedCart);

	return {
		subscribe,
		addProduct: (product: Product) => {
			update((val) => {
				let productExist = false;
				val.map((elm) => {
					if (elm.product.id === product.id) {
						productExist = true;
					}
					return elm;
				});

				if (!productExist) {
					val = [...val, { product }];
				}

				const tracker: undefined | any = get(matomo);
				if (tracker !== undefined) {
					val.forEach((product) => {
						tracker.addEcommerceItem(
							product.product.id,
							product.product.name,
							[],
							product.product.price
						);
					});
					const grandTotal = val.reduce((partialSum, x) => {
						return partialSum + x.product.price;
					}, 0);
					tracker.trackEcommerceCartUpdate(grandTotal);
				}

				return val;
			});
		},
		removeProduct: (product: Product) => {
			update((val) => {
				val = val.filter((elm) => elm.product.id !== product.id);
				const tracker: undefined | any = get(matomo);
				if (tracker !== undefined) {
					val.forEach((product) => {
						tracker.addEcommerceItem(
							product.product.id,
							product.product.name,
							[],
							product.product.price
						);
					});
					tracker.removeEcommerceItem(product.id);
					const grandTotal = val.reduce((partialSum, x) => {
						return partialSum + x.product.price;
					}, 0);
					tracker.trackEcommerceCartUpdate(grandTotal);
				}
				return val;
			});
		},
		refresh: async (products?: Product[] | undefined) => {
			const cartItems = get({ subscribe });
			let newCart: { product: Product }[] = [];
			if (products === undefined) {
				await Promise.all(
					cartItems.map(async (elm) => {
						const product = await (await fetch(`/api/products.json?id=${elm.product.id}`)).json();
						if (!Array.isArray(product)) {
							newCart = [...newCart, { product: product }];
						}
					})
				);
			} else {
				cartItems.forEach((elm) => {
					const newProduct = products.find((product) => product.id === elm.product.id);
					if (newProduct !== undefined) {
						newCart = [...newCart, { product: newProduct }];
					}
				});
			}
			newCart = newCart.filter((elm) => elm.product.inStock);

			const tracker: undefined | any = get(matomo);
			if (tracker !== undefined) {
				newCart.forEach((product) => {
					tracker.addEcommerceItem(
						product.product.id,
						product.product.name,
						[],
						product.product.price
					);
				});

				const difference = cartItems.filter(
					(x) => !newCart.some((y) => y.product.id === x.product.id)
				);
				difference.forEach((product) => {
					tracker.removeEcommerceItem(product.product.id);
				});

				const grandTotal = newCart.reduce((partialSum, x) => {
					return partialSum + x.product.price;
				}, 0);
				tracker.trackEcommerceCartUpdate(grandTotal);
			}

			set(newCart);
		},
		clear: () => {
			const tracker: undefined | any = get(matomo);
			if (tracker !== undefined) {
				tracker.clearEcommerceCart;
				tracker.trackEcommerceCartUpdate(0);
			}
			set([]);
		}
	};
};

export const createTipStore = async () => {
	const { subscribe, set } = writable<number | '20%'>(0);

	return {
		subscribe,
		set
	};
};

export const currentSong = createCurrentSongStore();
export const checkoutState = createCheckoutStateStore();
export const cart = await createCartStore();
export const tip = await createTipStore();
export const warningOn = writable<boolean>(false);
export const matomo = writable<undefined | any>(undefined);

cart.subscribe((val) => {
	if (browser) {
		localStorage.setItem(
			'cart',
			JSON.stringify(
				val.map((elm) => {
					return { id: elm.product.id };
				})
			)
		);
	}
});
