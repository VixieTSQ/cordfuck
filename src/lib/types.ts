export type Release = {
	catalogNumber: string;
	artist: string;
	title: string;
	review: string | undefined;
	reviewAuthor: string | undefined;
	description: string | undefined;
	date: Date;
	coverArtUrl: URL;
	downloadUrl: URL;
	songs: [Song];
};

export type Song = {
	trackNumber: number;
	artist: string;
	name: string;
	downloadUrl: URL;
};

export type Product = {
	releaseCatalogNumber: string | undefined;
	id: string;
	name: string;
	customsDeclarationExplanation: string;
	description: string;
	inStock: boolean;
	pictureUrl: URL;
	serviceLevel: string | undefined;
	serviceLevelInternational: string | undefined;
	parcel:
		| {
				length: string;
				width: string;
				height: string;
				distance_unit: string;
				weight: number;
				mass_unit: string;
		  }
		| undefined;
	price: number;
};

export type ShippingForm = {
	email: string;
	phone: string;
	country: string;
	firstName: string;
	lastName: string;
	address: string;
	addressTwo: string;
	city: string;
	zone: string;
	postalCode: string;
};
export const preValidateShippingForm = (
	values: ShippingForm,
	postalCodeExists: boolean,
	zoneExists: boolean,
	cityExists: boolean
) => {
	const errs: any = {};
	const deleteNoExistErrors = () => {
		for (const [key] of Object.entries(values)) {
			if (key === 'postalCode' && !postalCodeExists) {
				delete errs.postalCode;
			} else if (key === 'zone' && !zoneExists) {
				delete errs.zone;
			} else if (key === 'city' && !cityExists) {
				delete errs.city;
			}
		}
	};

	// Stage 1: no empty fields
	for (const [key, value] of Object.entries(values)) {
		if (value === '') {
			if (key === 'phone' || key === 'addressTwo') {
				continue;
			}
			errs[key] = 'This is required';
		}
	}

	deleteNoExistErrors();

	if (Object.keys(errs).length !== 0 && errs.constructor === Object) {
		return errs;
	}

	// Stage 2: no invalid fields
	if (
		!(values.phone === '') &&
		!/^[+]?[\s./0-9]*[(]?[0-9]{1,4}[)]?[-\s./0-9]*$/g.test(values.phone)
	) {
		errs.phone = 'Phone number must be valid';
	}
	if (
		!(values.email === '') &&
		!/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(
			values.email
		)
	) {
		errs.email = 'Email must be valid';
	}

	deleteNoExistErrors();
	return errs;
};

export type ShippingState = {
	kind: 'ShippingState';
	submissionRequested: boolean;
	shippingDoneCallback: (successful: boolean) => void;
};

export type PaymentState = {
	kind: 'PaymentState';
	submissionRequested: boolean;
	paymentDoneCallback: (successful: boolean) => void;
};
export type Shipment = {
	id: string;
	amount: number;
	estimatedDays: number | undefined;
	durationTerms: string | undefined;
};
export type ongoingOrderProduct = { shipment: Shipment; productId: string };

export type CheckoutState = {
	decrementStage: () => void;
	orderId: string | undefined;
	shippingForm: ShippingForm | undefined;
	ratePreview: ShippingRateResponse['ratePreview'] | undefined;
	stage: { kind: 'Cart' } | ShippingState | PaymentState | { kind: 'Done' };
};

export type ValidateAddressRequest = {
	products: { productId: string }[];
	form: ShippingForm;
	postalCodeExists: boolean;
	zoneExists: boolean;
	cityExists: boolean;
};

export type ShippingRateResponse = {
	errors: { type: 'address_error' | 'service_error'; text: string }[];
	orderId: string | undefined;
	ratePreview: {
		totalCost: number;
		rates: {
			estimatedDays: number | undefined;
		}[];
	};
};
