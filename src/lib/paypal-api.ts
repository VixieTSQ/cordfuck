import { SECRET_CORDFUCK_IS_PRODUCTION, SECRET_CORDFUCK_PAYPAL_SECRET } from '$env/static/private';
import { PUBLIC_CORDFUCK_PAYPAL_CLIENT_ID } from '$env/static/public';
import { error } from '@sveltejs/kit';

const PAYPALBASE =
	SECRET_CORDFUCK_IS_PRODUCTION === 'true'
		? 'https://api-m.paypal.com'
		: 'https://api-m.sandbox.paypal.com';

export const createOrder = async (body: any) => {
	const accessToken = await generateAccessToken();
	const response = await fetch(`${PAYPALBASE}/v2/checkout/orders`, {
		method: 'post',
		headers: {
			'Content-Type': 'application/json',
			Prefer: 'return=minimal',
			Authorization: `Bearer ${accessToken}`
		},
		body: JSON.stringify(body)
	});

	return (await handleResponse(response)).id;
};

export const retrieveOrder = async (id: string) => {
	const accessToken = await generateAccessToken();
	const response = await fetch(`${PAYPALBASE}/v2/checkout/orders/${id}`, {
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${accessToken}`
		}
	});

	return await handleResponse(response);
};

export const capturePayment = async (orderId: string) => {
	const accessToken = await generateAccessToken();
	const url = `${PAYPALBASE}/v2/checkout/orders/${orderId}/capture`;
	const response = await fetch(url, {
		method: 'post',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${accessToken}`
		}
	});
	return await handleResponse(response);
};

export const generateClientToken = async () => {
	const accessToken = await generateAccessToken();
	const response = await fetch(`${PAYPALBASE}/v1/identity/generate-token`, {
		method: 'post',
		headers: {
			Authorization: `Bearer ${accessToken}`,
			'Accept-Language': 'en_US',
			'Content-Type': 'application/json',
			Prefer: 'return=representation'
		}
	});
	return (await handleResponse(response)).client_token;
};

const generateAccessToken = async () => {
	const auth = Buffer.from(
		PUBLIC_CORDFUCK_PAYPAL_CLIENT_ID + ':' + SECRET_CORDFUCK_PAYPAL_SECRET
	).toString('base64');
	const response = await fetch(`${PAYPALBASE}/v1/oauth2/token`, {
		method: 'post',
		body: 'grant_type=client_credentials',
		headers: {
			Authorization: `Basic ${auth}`
		}
	});
	return (await handleResponse(response)).access_token;
};

const handleResponse = async (response: any) => {
	if (response.ok) {
		return response.json();
	} else {
		throw error(502, await response.text());
	}
};
