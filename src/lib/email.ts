import emailHtml from '$lib/assets/tracking-email.html?raw';
import type { Product } from '$lib/types.js';
import {
	SECRET_CORDFUCK_EMAIL_HOST,
	SECRET_CORDFUCK_EMAIL_PORT,
	SECRET_CORDFUCK_EMAIL_USER,
	SECRET_CORDFUCK_EMAIL_PASS
} from '$env/static/private';
import nodemailer from 'nodemailer';
import type { ongoingOrder } from '$lib/db.js';

const email = nodemailer.createTransport({
	host: SECRET_CORDFUCK_EMAIL_HOST,
	port: SECRET_CORDFUCK_EMAIL_PORT,
	secure: true,
	auth: {
		user: SECRET_CORDFUCK_EMAIL_USER,
		pass: SECRET_CORDFUCK_EMAIL_PASS
	}
});

export const sendTrackingEmail = (
	to: string,
	products: { product: Product }[],
	trackingCodes: string[]
) => {
	// Construct email html using the emailHtml template. Also construct plain text version
	const codesHtml = trackingCodes.reduce((partialSum, code) => {
		return (
			partialSum +
			`<!--[if true]>
        <td width="100%">
        <![endif]-->
        <!--[if !true]><!-->
        <div style="display:inline-block;width:100%">
            <!--<![endif]-->
            <a style="margin: 1em 0;" href="https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=${code}">${code}</a>
            <!--[if !true]><!-->
        </div>
        <!--<![endif]-->
        <!--[if true]>
        </td>
        <![endif]-->
`
		);
	}, '');
	const codesPlain = trackingCodes.reduce((partialSum, code) => {
		return partialSum + code + '\n';
	}, '');

	const productsHtml = products.reduce((partialSum, product) => {
		return (
			partialSum +
			`<!--[if true]>
        <td width="100%">
        <![endif]-->
        <!--[if !true]><!-->
        <div style="display:inline-block;width:100%">
            <!--<![endif]-->
            ${product.product.name}
            <!--[if !true]><!-->
        </div>
        <!--<![endif]-->
        <!--[if true]>
        </td>
        <![endif]-->
`
		);
	}, '');
	const productsPlain = products.reduce((partialSum, product) => {
		return partialSum + `${product.product.name}\n`;
	}, '');

	let html = emailHtml.replace('TRACKING_CODES_PLACEHOLDER', codesHtml);
	html = html.replace('PRODUCTS_PLACEHOLDER', productsHtml);

	const plainText = `You ordered:
${productsPlain}

Your tracking number(s) are:
${codesPlain}

Track your order(s) at https://tools.usps.com/go/TrackConfirmAction_input
It may take a few days as your order gets produced before it starts shipping. If you're receiving your shipment outside the US you may be subject to extra duties and import taxes from your country's postal system. You may also lose tracking at some point.

If you have any questions, contact us at contact@cordfuck.com`;

	email.sendMail({
		from: `CordFuck <${SECRET_CORDFUCK_EMAIL_USER}>`,
		to,
		subject: 'Your CordFuck order...',
		html,
		text: plainText
	});
};

export const sendErrorEmail = (
	buyerEmail: string,
	err: any,
	order: ongoingOrder,
	orderId: string
) => {
	const plainText = `The buyer's email is: ${buyerEmail}

The error details:
${err}

Full order details:
${JSON.stringify(order)}`;

	email.sendMail({
		from: `CordFuck <${SECRET_CORDFUCK_EMAIL_USER}>`,
		to: SECRET_CORDFUCK_EMAIL_USER,
		subject: `An error occurred with order ${orderId}`,
		text: plainText
	});
};
