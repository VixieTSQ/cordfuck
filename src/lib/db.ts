import * as fs from 'fs/promises';
import {
	SECRET_CORDFUCK_RELEASE_DB_PATH,
	SECRET_CORDFUCK_PRODUCTS_DB_PATH,
	SECRET_CORDFUCK_SHIPPO_API_KEY
} from '$env/static/private';
import type { Product, Release, ShippingForm } from './types.js';
import Shippo from 'shippo';
import { error } from '@sveltejs/kit';

const hourInMs = 3600000;

export const shippo = Shippo(SECRET_CORDFUCK_SHIPPO_API_KEY);

export type ongoingOrder = {
	address: ShippingForm;
	cart: { product: Product; shippingRateId: string | undefined }[];
	tip: number | undefined;
	timeCreated: number;
	postTransactionJobPromise?: Promise<void> | undefined;
};
export const ongoingOrders = new Map<string, ongoingOrder>();

const removeStaleOngoingOrders = async () => {
	const dayAgo = Date.now() - hourInMs * 24;
	let postTransactionPromises: Promise<void>[] = [];
	ongoingOrders.forEach((order, id) => {
		if (order.timeCreated < dayAgo) {
			if (order.postTransactionJobPromise !== undefined) {
				postTransactionPromises = [...postTransactionPromises, order.postTransactionJobPromise];
			}
			ongoingOrders.delete(id);
		}
	});
	await Promise.all(postTransactionPromises);
};
setInterval(removeStaleOngoingOrders, hourInMs);

export const getReleases = async (): Promise<Release[]> => {
	const db = await fs.open(SECRET_CORDFUCK_RELEASE_DB_PATH, 'r');
	const raw = (await db.readFile()).toString();
	db.close();

	return JSON.parse(raw);
};

export const getProducts = async (): Promise<Product[]> => {
	const db = await fs.open(SECRET_CORDFUCK_PRODUCTS_DB_PATH, 'r');
	const raw = (await db.readFile()).toString();
	db.close();

	return JSON.parse(raw);
};

interface HasProductId {
	productId: string;
}

export const getProductsByIds = async <T extends HasProductId>(
	ids: T[]
): Promise<(T & { product: Product })[]> => {
	const allProducts = await getProducts();

	let selectedProducts: (T & { product: Product })[] = [];
	ids.forEach((requestedProduct) => {
		const selectedProduct = allProducts.find(
			(product) => product.id === requestedProduct.productId
		);
		if (selectedProduct !== undefined && selectedProduct.inStock) {
			selectedProducts = [
				...selectedProducts,
				{
					...requestedProduct,
					product: selectedProduct
				}
			];
		} else {
			throw error(
				400,
				`Product with ID ${requestedProduct.productId} does not exist or isn't in stock`
			);
		}
	});

	return selectedProducts;
};
