export const toPrettyTime = (rawTime: number): string => {
	const hrs = Math.floor(rawTime / 60 / 60);
	const mins = Math.floor((rawTime / 60) % 60);
	const secs = Math.floor(rawTime - hrs * 60 * 60 - mins * 60);

	let durPrettySecs = secs.toString();
	let durPrettyMins = mins.toString();

	if (secs < 10) {
		durPrettySecs = `0${secs}`;
	}
	if (mins < 10) {
		durPrettyMins = `0${mins}`;
	}

	if (hrs) {
		return `${hrs}:${durPrettyMins}:${durPrettySecs}`;
	} else {
		return `${durPrettyMins}:${durPrettySecs}`;
	}
};
