import type { Release } from '$lib/types.js';
import type { PageLoad } from './$types.js';

export const load: PageLoad = async ({ fetch }) => {
	const release_res = await fetch('/api/discography.json');
	const releases: Release[] = await release_res.json();

	return {
		release: releases[0],
	};
};
