import type { RequestHandler } from '@sveltejs/kit';
import { getReleases } from '$lib/db';
import sanitizeHtml from 'sanitize-html'

const top = `<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">

<channel>
  <title>CordFuck discography</title>
  <link>https://cordfuck.com/discography</link>
  <description>The entire CordFuck discography</description>
`

const bottom = `
</channel>

</rss>`

export const GET: RequestHandler = async () => {
    const releases = await getReleases();

    const items = releases.reduce((items, release) => {
        return items + `
        <item>
            <title>${sanitizeHtml(release.artist)} - ${sanitizeHtml(release.title)}</title>
            <link>https://cordfuck.com/discography/${release.catalogNumber}</link>
            <description>${release.review !== undefined && release.reviewAuthor !== undefined ? sanitizeHtml("\"" + release.review + '" - ' + release.reviewAuthor + '\n\n') : ''}${release.description !== undefined ? sanitizeHtml(release.description) + '\n' : ''}Download at ${sanitizeHtml(release.downloadUrl.toString())}</description>
        </item>
        `
    }, "")

    const rss = top + items + bottom;

    return new Response(rss, {
        status: 200,
        headers: new Headers({
            'Content-type': 'application/rss+xml',
            'Access-Control-Allow-Origin': '*'
        })
    });
};
