import type { Release } from '$lib/types.js';
import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types.js';

export const load: PageLoad = async ({ fetch, params }) => {
	const releaseRes = await fetch(`/api/discography.json?cat=${params.catnum}`);
	const release: Release | null = await releaseRes.json();
	if (release === null) {
		throw error(404, `Release ${params.catnum} does not exist`);
	}

	return {
		release
	};
};
