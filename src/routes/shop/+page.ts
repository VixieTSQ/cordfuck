import type { Product } from '$lib/types.js';
import type { PageLoad } from './$types.js';

export const load: PageLoad = async ({ fetch }) => {
	const res = await fetch('/api/products.json');
	const products: Product[] = await res.json();

	return { products };
};
