import { getReleases } from '$lib/db.js';
import type { Release } from '$lib/types.js';
import { error, type RequestHandler } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ url }) => {
	let res: Release[];
	try {
		res = await getReleases();
	} catch (e) {
		console.error(e);
		throw error(500, 'Database error');
	}

	let releases: Release[] | Release = [];
	const cat = url.searchParams.get('cat');

	if (cat !== null) {
		res.every((elm) => {
			if (elm.catalogNumber === cat) {
				releases = elm;
				return false;
			}
			return true;
		});
	} else {
		releases = res;
	}

	return new Response(JSON.stringify(releases), {
		status: 200,
		headers: new Headers({
			'Content-type': 'application/json',
			'Access-Control-Allow-Origin': '*'
		})
	});
};
