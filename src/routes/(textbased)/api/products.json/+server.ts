import { getProducts } from '$lib/db.js';
import type { Product } from '$lib/types.js';
import { error, type RequestHandler } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ url }) => {
	let res: Product[];
	try {
		res = await getProducts();
	} catch (e) {
		console.error(e);
		throw error(500, 'Database error');
	}

	let products: Product[] | Product = [];
	const id = url.searchParams.get('id');

	if (id !== null) {
		res.every((elm) => {
			if (elm.id === id) {
				products = elm;
				return false;
			}
			return true;
		});
	} else {
		products = res;
	}

	return new Response(JSON.stringify(products), {
		status: 200,
		headers: new Headers({
			'Content-type': 'application/json',
			'Access-Control-Allow-Origin': '*'
		})
	});
};
