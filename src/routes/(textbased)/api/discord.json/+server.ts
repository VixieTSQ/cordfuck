import type { RequestHandler } from '@sveltejs/kit';

const inviteCode = 'CYbKDrgref';

export const GET: RequestHandler = async () => {
	let res;
	try {
		res = await (
			await fetch(`https://discord.com/api/v8/invites/${inviteCode}?with_counts=true`)
		).json();
	} catch {
		res = { approximate_member_count: 0, approximate_presence_count: 0 };
	}

	return new Response(JSON.stringify(res), {
		status: 200,
		headers: new Headers({
			'Content-type': 'application/json',
			'Access-Control-Allow-Origin': '*'
		})
	});
};
