import { getReleases } from '$lib/db.js';
import { error, type RequestHandler } from '@sveltejs/kit';
import template from '$lib/assets/release-og.html?raw';
import { Resvg } from '@resvg/resvg-js';
import satori from 'satori';
import { html } from 'satori-html';
import probe from 'probe-image-size';

const boldFile = await fetch(
	'https://github.com/sorenson/open-sans-woff/raw/master/fonts/Bold/OpenSans-Bold.woff'
);
const boldData: ArrayBuffer = await boldFile.arrayBuffer();
const semiBoldFile = await fetch(
	'https://github.com/sorenson/open-sans-woff/raw/master/fonts/Semibold/OpenSans-Semibold.woff'
);
const semiBoldData: ArrayBuffer = await semiBoldFile.arrayBuffer();
const extraBoldFile = await fetch(
	'https://github.com/sorenson/open-sans-woff/raw/master/fonts/ExtraBold/OpenSans-ExtraBold.woff'
);
const extraBoldData: ArrayBuffer = await extraBoldFile.arrayBuffer();

export const GET: RequestHandler = async ({ url }) => {
	const catalogNumber = url.searchParams.get('cat');
	if (catalogNumber === null) {
		throw error(400, `Did not provide cat parameter`);
	}
	const release = (await getReleases()).find((release) => release.catalogNumber === catalogNumber);
	if (release === undefined) {
		throw error(400, `Release with catalog number ${catalogNumber} does not exist`);
	}

	const coverImageDimensions = await probe(release.coverArtUrl.toString());
	const width =
		coverImageDimensions.width! >= coverImageDimensions.height! ? 566 : coverImageDimensions.width;
	const height =
		coverImageDimensions.width! <= coverImageDimensions.height! ? 566 : coverImageDimensions.height;

	const finishedHtml = template
		.replace('IMAGEHEIGHT', height!.toString())
		.replace('IMAGEWIDTH', width!.toString())
		.replace('RELEASEURL', release.coverArtUrl.toString())
		.replace('RELEASETITLE', release.title)
		.replace('RELEASEARTIST', release.artist);
	const svg = await satori(html(finishedHtml), {
		width: 1200,
		height: 630,
		fonts: [
			{
				name: 'Open Sans',
				weight: 700,
				data: Buffer.from(boldData)
			},
			{
				name: 'Open Sans',
				weight: 600,
				data: Buffer.from(semiBoldData)
			},
			{
				name: 'Open Sans',
				weight: 800,
				data: Buffer.from(extraBoldData)
			}
		]
	});
	const image = new Resvg(svg, {
		fitTo: {
			mode: 'width',
			value: 1200
		}
	})
		.render()
		.asPng();

	// return new Response(svg, {
	//     status: 200,
	//     headers: new Headers({
	//         'Content-type': 'image/svg+xml',
	//         'Access-Control-Allow-Origin': '*'
	//     })
	// });
	return new Response(image, {
		status: 200,
		headers: new Headers({
			'Content-type': 'image/png',
			'Access-Control-Allow-Origin': '*'
		})
	});
};
