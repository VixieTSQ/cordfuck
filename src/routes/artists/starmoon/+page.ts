import type { Release } from '$lib/types.js';
import type { PageLoad } from './$types.js';

export const load: PageLoad = async ({ fetch }) => {
	const res = await fetch('/api/discography.json');
	let releases: Release[] = await res.json();

	releases = releases.filter((val) => {
		return val.artist.includes('starmoon');
	});

	return { releases };
};
