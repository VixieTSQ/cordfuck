# CordFuck

![GitLab](https://img.shields.io/gitlab/license/VixieTSQ/cordfuck?style=for-the-badge)

This is the website for CordFuck! The underground electronic music label.

## Usage

### .env

First fill out the `example_env` file and copy it to a new `.env` file. Examples for what the database files look like can be found in the `products-testdb.json` and `releases-testdb.json` files.

### Building

To compile the program, run the following:

```
npm install
npm run build
```

### Running

To finally run the program, run `node build`. This runs node on the `build` directory created above. Using Nginx as downstream is recommended as TLS is neccessary for this site to make sense.

## License

All code is licensed under GNU Affero General Public License 3.0 or later. All art unless specified otherwise licensed under CC-BY-SA 4.0
